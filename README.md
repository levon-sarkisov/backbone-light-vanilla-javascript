# README #

Small lib as Backbone.js ;)
====

For run this project you need installed *nodejs*
After downloading project in root directory make next steps:

Open terminal and run

* *npm install*
* *npm start*
* *open in browser http://localhost:3000*

For tests, you have to installed Firefox browser
* *npm test*

The project was tested in IE9<
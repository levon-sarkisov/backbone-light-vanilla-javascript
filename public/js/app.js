'use strict';

var BB = {};

BB.model = new Mockup.Model({});

BB.view = new Mockup.View({

    showItemsLength: function(obj) {

        var text = obj.data + ' items';

        /**
         * If items length more than 2
         * removes 's' of 'items'
         */
        
        if(obj.data < 2) {
            text = obj.data + ' item';
        }

        var len = [{
            len: text
        }];

        var htm = this.template(obj.tmp, len);
        // I know, that innerHTML is bad ;)
        obj.items.innerHTML = htm;
    },

    addNewItem: function(obj) {
        var htm = this.template(obj.tmp, obj.data);
        obj.items.innerHTML = htm;
    }
});

BB.ctrl = new Mockup.Controller({

    view: BB.view,

    model: BB.model,

    events: {
        '#add click': 'add',
        '#items click': 'delete'
    },

    init: function() {

        /**
         * Cached DOM elements
         */

        this.newItem = this.id('new-item');
        this.items = this.id('items');
        this.tmp = this.id('items-tmp');
        this.itemNum = this.id('items-number');
        this.itemNumTmp = this.id('items-number-tmp');

        this.render();
    },

    /**
     * The render function will call
     * each time, when model is updated
     */

    render: function() {
        var self = BB.ctrl || this;

        self.view.addNewItem({
            items: self.items,
            tmp: self.tmp,
            data: self.model.data
        });

        self.view.showItemsLength({
            items: self.itemNum,
            tmp: self.itemNumTmp,
            data: self.model.data.length
        });
    },

    add: function(evt, self) {
        evt.preventDefault();

        var val = self.newItem.value;

        if(!val) {
            return false;
        }
        
        self.model.set({
            item: val
        });
    },

    delete: function(evt, self) {
        evt.preventDefault();

        var id = evt.target.getAttribute('data-id');
        self.model.remove(id);
    }
});
















